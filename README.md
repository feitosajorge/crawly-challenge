# Crawly Challenge

Crawler using Java and Maven

## Requirements

- [JDK 1.8](https://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)
- [Maven (Version used but not required: 3.5.2)](https://maven.apache.org/download.cgi)
- Alternative: [Download Netbeans IDE which comes up with the JDK and Maven](https://www.oracle.com/technetwork/pt/java/javase/downloads/jdk-netbeans-jsp-3413153-ptb.html)

## How to execute

### Method 1:

- Go to root folder ``crawly-challenge``

- Execute: ``mvn clean package``
	- If any failure happens, maybe it is necessary to delete your repository folder located in .m2 folder
- Execute: ``mvn exec:java``

![Output example](https://i.imgur.com/PhyjIjs.png)


### Method 2:

- Import the project to Netbeans IDE
- Execute the Crawler.java class

![Output example](https://i.imgur.com/bcowOL6.png) 
