package com.crawly.challenge;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;
import org.apache.http.HttpResponse;

/**
 * Class to convert any necessary data to get to the correct answer
 */
public class Converter {

    /**
     * The token characters to be replaced, based on the adpagespeed.js file
     * found in the URL
     */
    private final Map<Character, Character> replacements = new HashMap<Character, Character>() {
        {
            put('a', 'z');
            put('b', 'y');
            put('c', 'x');
            put('d', 'w');
            put('e', 'v');
            put('f', 'u');
            put('g', 't');
            put('h', 's');
            put('i', 'r');
            put('j', 'q');
            put('k', 'p');
            put('l', 'o');
            put('m', 'n');
            put('n', 'm');
            put('o', 'l');
            put('p', 'k');
            put('q', 'j');
            put('r', 'i');
            put('s', 'h');
            put('t', 'g');
            put('u', 'f');
            put('v', 'e');
            put('w', 'd');
            put('x', 'c');
            put('y', 'b');
            put('z', 'a');
        }
    };

    /**
     * Get the current state of the token found in the hidden input and change
     * its characters based on the rule stablished in the adpagespeed.js file
     * available in the <code>scipt</code> tag
     *
     * @param currentToken the token found in the hidden input
     * @return the token with the replaced characters, in String format
     */
    public String findAnswer(String currentToken) {
        String newToken = "";

        for (int i = 0; i < currentToken.length(); i++) {
            newToken += getReplacements().containsKey(currentToken.charAt(i)) ? getReplacements().get(currentToken.charAt(i)) : currentToken.charAt(i);
        }

        return newToken;
    }

    /**
     * Convert the downloaded content to String
     * @param response the content downloaded in <code>HttpResponse</code> format
     * @return the HTTP Response content converted to String
     * @throws IOException Signals that an I/O exception of some sort has occurred
     */
    public String httpResponseToString(HttpResponse response) throws IOException {
        BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

        StringBuilder result = new StringBuilder();
        String line;
        while ((line = rd.readLine()) != null) {
            result.append(line);
        }

        return result.toString();
    }

    /**
     * Get the map characters to be replaced
     * @return the characters to be replaced in map format
     */
    public Map<Character, Character> getReplacements() {
        return replacements;
    }

}
