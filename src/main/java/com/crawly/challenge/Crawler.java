package com.crawly.challenge;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

/**
 * Main class to execute the main functions for the crawly company test
 */
public class Crawler {

    /**
     * The URL to be downloaded
     */
    private final String url = "http://applicant-test.us-east-1.elasticbeanstalk.com/";

    public static void main(String[] args) throws IOException {

        Crawler crawler = new Crawler();

        Converter converter = new Converter();

        HttpClient client = HttpClientBuilder.create().build();

        HttpPost request = new HttpPost(crawler.getUrl());
        HttpResponse response = client.execute(request);

        String discoverResult = converter.httpResponseToString(response);

        Document discoverDocument = Jsoup.parse(discoverResult);
        Element tokenElement = discoverDocument.select("#token").first();

        List<NameValuePair> param = new ArrayList<>(1);
        param.add(new BasicNameValuePair("token", converter.findAnswer(tokenElement.attr("value"))));
        request.setEntity(new UrlEncodedFormEntity(param, Charset.forName("UTF-8")));

        HttpResponse post_response = client.execute(request);

        String finalResult = converter.httpResponseToString(post_response);

        Document discoveredDocument = Jsoup.parse(finalResult);
        
        System.err.println("##################################################");
        System.err.println(discoveredDocument.text());
        System.err.println("##################################################");

    }

    /**
     * The URL to be downloaded
     * @return the URL in String format
     */
    public String getUrl() {
        return url;
    }

}
